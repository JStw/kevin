$Path = "C:"

# On test si le chemin donn�e existe
If(Test-Path $Path -ErrorAction SilentlyContinue)
{
    # On r�cup�re tous les dossiers "Users" dans le chemin donn�e.
    $users = Get-ChildItem -Directory -Path "$Path\Users"

    
    # Pour chaque utilisateur, on va r�cup�rer le contenu du dossier "Downloads"
    ForEach($User in $users)
    {
       
        # On cherche les fichiers (pas les dossiers) qui sont des copies, on utilise une expression r�guli�re
        # en v�rifiant si le fichier a comme nom: << nom du fichier >> (<< num�ro de copie >>).<< extension>>
        # ou bien si le fichier a comme nom: << nom du fichier >> - Copy.<< extension >>
        $files = Get-ChildItem -Path "$Path\Users\$User\Downloads" -Recurse | Where {!$_.PSIsContainer} | Where-Object{($_.name -match '^(.*)(\(\d\)|- Copy|- Copie)\.(.*)$') -or ($_.LastWriteTime -lt (Get-Date).AddMonths(-3))} | Sort Length

        # Si on obtient des fichiers
        If ($files.Count -gt 0) {

            # Alors on pr�pare des variables pour calculer le nombre de fichier supprim�s, la progression courante
            # l'espace lib�r� apr�s suppression etc.
            $EachFile = 0; $TotalFiles = $files.Count
            $EachFile = [Math]::Round((100/$TotalFiles),2)
            $CurrentProgress = 0; $FilesChecked = 0
            $FilesDeleted = 0; $DeletedFileNames = ""
            [Int64]$SpaceCreated = 0; $SpaceFreed = ""

            # Enfin pour chaque fichier
            ForEach($File In $files)
            {
                Write-Progress -Activity "Checking duplicates for total $TotalFiles files - ($FilesChecked files) $CurrentProgress% complete" -CurrentOperation  "currentfile file $($File.FullName)" -PercentComplete $CurrentProgress
            
                # On supprime le fichier en question.
                $DeletedFileNames += "$File`n"
                Remove-Item -Path $File.FullName -Force


                # On calcule l'espace lib�r�, puis on incr�mente le nombre de fichier supprim� apr�s suppression.
                $SpaceCreated = $SpaceCreated + $File.Length
                $FilesDeleted++

                # On calcul la progression et le nombre de fichier v�rifi�.
                $CurrentProgress = [Math]::Round(($CurrentProgress + $EachFile),2)
                If($CurrentProgress -gt 100){$CurrentProgress = 100}
                $FilesChecked++
            }

            Write-Progress -Activity "All done" -Completed

            # On formate ensuite l'espace lib�r� pour que �a soit plus parlant (KB, MB, GB etc.)
            Switch ($SpaceCreated)
            {
                { $_ -gt 0 -and $_ -le 1024 }
                {
                    $SpaceFreed = "$([Math]::Round(($SpaceCreated),2)) Bytes"
                }
                { $_ -gt 1024 -and $_ -le 1048576 }
                {
                    $SpaceFreed = "$([Math]::Round(($SpaceCreated/1Kb),2)) KB"
                }
                { $_ -gt 1048576 -and $_ -le 1073741824 }
                {
                    $SpaceFreed = "$([Math]::Round(($SpaceCreated/1MB),2)) MB"
                }
                { $_ -gt 1073741824 }
                {
                    $SpaceFreed = "$([Math]::Round(($SpaceCreated/1GB),2)) GB"
                }
                Default
                {
                    $SpaceFreed = "$([Math]::Round(($SpaceCreated),2)) Bytes"
                }
            }

            # On �crit le sommaire de ce qui a �t� fait pour chaque dossier User/Downloads
            Write-Host "Summary for user $User `r`n$('='*50)`r`nTotal files scanned:`t$FilesChecked`r`nTotal files deleted:`t$FilesDeleted`r`nTotal Space created:`t$SpaceFreed`r`n$('='*50)" -ForegroundColor Yellow

            # On affiche les fichiers supprim�s
            If($DeletedFileNames -ne "")
            {
                Write-Host "Below duplicate files were deleted:" -ForegroundColor Yellow
                Write-Output $DeletedFileNames
            }
            Else
            {
                Write-Host "No duplicate files were found or deteled" -ForegroundColor Green
            }
        }
        Else
        {
            Write-Host "No files to clean for user $User" -ForegroundColor Green
        }

        # Cleaning empty directories
        do {
            $dirs = gci "$Path\Users\$User\Downloads" -directory -recurse | Where { (gci $_.fullName -Force).count -eq 0 } | select -expandproperty FullName
            $dirs | Foreach-Object { Remove-Item $_ }
        } while ($dirs.count -gt 0)
    }
}
Else
{
    Write-Error "Path not found '$Path'"
}